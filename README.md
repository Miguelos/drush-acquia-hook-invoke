# drush-acquia-hook-invoke

This is a Drush command designed to let you test execution of [Acquia Cloud hooks](https://github.com/acquia/cloud-hooks) on your local Drupal installation (for development or debugging, etc.).

## Requirements

* Must be run from a Drupal installation that is inside a Git repository.
* There must be a [directory called `hooks`](https://github.com/acquia/cloud-hooks#the-cloud-hooks-directory) in the root Git repository.
* Ideally you run this using a Drush alias like `@sitename.local` that mirrors the syntax of the Acquia site aliases.

## Usage

Ideally you run this using a Drush alias like `@sitename.local` that mirrors the syntax of the Acquia site aliases.

```bash
$ drush @mysite.local post-code-deploy --source-branch=master --deployed-tag=test-tag
```

For more help about the available options, see `drush help ac-hook-invoke`:

```
Invoke an Acquia cloud hook on the current local environment.

Examples:
 drush @mysite.local post-code-deploy      Run the post-code-deploy hook scripts. 
 --source-branch=master                                                       
 --deployed-tag=test-tag

Arguments:
 hook                                      The hook to invoke. For example, post-code-deploy

Options:
 --ac-env                                  The environment to which code was just deployed.                           
 --ac-site                                 The site name. This is the same as the Acquia Cloud username for the site. 
 --deployed-tag                            The code branch or tag being deployed.                                     
 --repo-type                               The version control system your site is using; "git" or "svn".             
 --repo-url                                The URL of your code repository.                                           
 --source-branch                           The code branch or tag being deployed.
```