<?php

/**
 * Implements hook_drush_command().
 */
function acquia_hook_invoke_drush_command() {
  $info['ac-hook-invoke'] = array(
    'description' => 'Invoke an Acquia cloud hook on the current local environment.',
    'callback' => 'drush_acquia_hook_invoke_callback',
    'arguments' => array(
      'hook' => 'The hook to invoke. For example, post-code-deploy',
    ),
    'required-arguments' => TRUE,
    'options' => array(
      'ac-site' => 'The site name. This is the same as the Acquia Cloud username for the site.',
      'ac-env' => 'The environment to which code was just deployed.',
      'source-branch' => 'The code branch or tag being deployed.',
      'deployed-tag' => 'The code branch or tag being deployed.',
      'repo-url' => 'The URL of your code repository.',
      'repo-type' => 'The version control system your site is using; "git" or "svn".',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_CONFIGURATION,
    'category' => 'acapi',
    'examples' => array(
      'drush @mysite.local post-code-deploy --source-branch=master --deployed-tag="test-tag"' => 'Run the post-code-deploy hook scripts.',
    ),
  );

  return $info;
}

/**
 * Implements drush_hook_COMMAND_validate().
 */
function drush_acquia_hook_invoke_ac_hook_invoke_validate($hook) {
  if (!drush_get_context('DRUSH_REPO_ROOT')) {
    if ($repo_root = _drush_acquia_hook_find_repo_root()) {
      drush_set_context('DRUSH_REPO_ROOT', $repo_root);
    }
    else {
      return drush_set_error('NOT_GIT_REPO', dt('The current directory is not a Git repo.'));
    }
  }

  $hook_dir = drush_get_context('DRUSH_REPO_ROOT') . '/hooks';
  if (!is_dir($hook_dir)) {
    return drush_set_error('HOOKS_DIR_NOT_FOUND', dt('The hooks directory @dir does not exist.', array('@dir' => $hook_dir)));
  }

  $valid_hooks = array(
    'post-code-deploy',
    'post-code-update',
    'post-db-copy',
    'post-files-copy',
  );
  if (!in_array($hook, $valid_hooks)) {
    // Only return a notice instead of an error.
    drush_log(dt('@hook is not a valid Acquia Cloud hook.', array('@hook' => $hook)), 'warning');
  }
}

/**
 * Drush callback for the ac-hook-invoke command.
 */
function drush_acquia_hook_invoke_callback($hook) {
  $hook_dir = drush_get_context('DRUSH_REPO_ROOT') . '/hooks';

  $dirs = array();
  $dirs[] = $hook_dir . '/' . $_ENV['AH_SITE_ENVIRONMENT'] . '/' . $hook;
  $dirs[] = $hook_dir . '/common/' . $hook;
  $scripts = array();
  foreach ($dirs as $dir) {
    drush_log(dt('Searching for scripts in @dir', array('@dir' => $dir)), 'debug');
    $files = drush_scan_directory($dir, '/.*/');
    foreach ($files as $file) {
      if (is_executable($file->filename)) {
        $scripts[] = $file->filename;
      }
      else {
        drush_log(dt('Script file @file was not executable.', array('@file' => $file->filename)), 'debug');
      }
    }
  }

  if (empty($scripts)) {
    return drush_set_error('NO_SCRIPTS', dt('Unable to find any scripts to invoke for the @hook hook.', array('@hook' => $hook)));
  }

  // Attempt to use the target alias as the defaults for site and environment.
  // This assumes that the alias has a format of @site.environment
  if (!drush_get_option('ac-site') && !drush_get_option('ac-env') && $alias = drush_get_context('DRUSH_TARGET_SITE_ALIAS')) {
    if (strpos($alias, '.') > 0) {
      list($site, $env) = explode('.', ltrim($alias, '@'), 2);
      drush_set_option('ac-site', $site);
      drush_set_option('ac-env', $env);
    }
  }

  $site = drush_get_option('ac-site', isset($_ENV['AH_SITE_NAME']) ? $_ENV['AH_SITE_NAME'] : 'unknown-site');
  $target_env = drush_get_option('ac-env', isset($_ENV['AH_SITE_ENVIRONMENT']) ? $_ENV['AH_SITE_ENVIRONMENT'] : 'unknown-env');
  $source_branch = drush_get_option('source-branch', 'unknown-branch');
  $deployed_tag = drush_get_option('deployed-tag', 'unknown-tag');
  $repo_url = drush_get_option('repo-url', 'unknown-url');
  $repo_type = drush_get_option('repo-type', 'git');

  //$site_root = drush_get_context('DRUSH_SELECTED_DRUPAL_ROOT');
  //$cwd = getcwd();
  //drush_op('chdir', $site_root);

  foreach ($scripts as $script) {
    $command = $script . ' %s %s %s %s %s %s';
    drush_shell_exec_interactive($command, $site, $target_env, $source_branch, $deployed_tag, $repo_url, $repo_type);
  }

  //drush_op('chdir', $cwd);
}


/**
 * Attempt to find the root directory of a Git clone.
 *
 * @return string|bool
 *   The Git root directory if found, or FALSE otherwise.
 */
function _drush_acquia_hook_find_repo_root() {
  if ($root = drush_get_option('root')) {
    $directory = $root;
  }
  else {
    $directory = drush_cwd();
  }

  if (!is_dir($directory)) {
    return FALSE;
  }

  $result = FALSE;
  $original_simulate = drush_get_context('DRUSH_SIMULATE');
  drush_set_context('DRUSH_SIMULATE', FALSE);
  $success = drush_shell_cd_and_exec($directory, 'git rev-parse --show-toplevel 2> ' . drush_bit_bucket());
  if ($success) {
    $output = drush_shell_exec_output();
    $result = $output[0];
  }
  drush_set_context('DRUSH_SIMULATE', $original_simulate);
  return $result;
}
